# WormHugs.Info

The purpose of WormHugs.info is to expose abusive and defamatory practices of tulpa.info staff to the community.

This repository contains changes _since website's publication_ in WormHugs.info domain, not the whole work. One of the reason being: there are some things that I included at first and eventually abstained to share.

Precisely some DMs content I decided in the end I don't need to share to prove my point as the evidence consisting of publicly available messages is already overwhelming.

It's a simple static website built with [Zola](https://www.getzola.org/) SSG and [Bulma](https://bulma.io/) CSS framework, nothing fancy.