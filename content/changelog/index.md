+++
title = "Changelog"
+++

This page is going to document changes made **after** release of the page in public, in [WormHugs.info](wormhugs.info) domain. All changes since release are also exposed in [Gitlab repository](https://gitlab.com/LapisReginaFaux/wormhugs.info-released)

---

- **2024-03-18** -- an update at the top of main website and a few minor typos I noticed why reading it after some time.