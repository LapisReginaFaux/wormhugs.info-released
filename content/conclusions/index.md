+++
title = "Conclusions"
template = "conclusions.html"
+++

Here is a list of semi-formal conclusions proving my point in the case. They are semi-formal in a way that:
- Each conclusion is explicitly verifiable or falsifiable
  - Verifiable in a way that it's based on explicit evidence or other conclusion. In other words, it can be denied by proving those evidence or deriving conclusions wrong.
  - Falsifiable in a way that explicit lack of evidence on a concrete facts implies it. It can be denied by providing evidence on that fact. For example, {{ conc(id="C001") }} is falsifiable as it can be disproven by providing evidence on Kanade giving headpats to An after An has drawn her boundaries.
- Conclusions' dependencies on other conclusions don't make cycles.