[C001]
title = "All Kanade's actual headpats given to An and asking happened before An has drawn any boundaries"
description = """
All of those documented in the report and found by me happened in {{ event(id="e001") }} which happened before {{ sevent(id="se003") }}
"""
falsifiability = """
Find events of Kanade headpatting An or asking her to let Kanade give her a headpat after {{ sevent(id="se003") }}
"""

[C002]
title = "All instances of us stepping on An's boundaries happened as reactions to her being mean to us AND already talking to us about headpats"
description = """
In {{ event(id="e007") }} Kanade, after being asked why she doesn't let me talk about headpats and called a gatekeeper, tells An she can ask for a headpat from me as she definitely isn't getting one from Kanade. Interpreting it as an offer An could feel bad to refuse is as farfetched as interpreting "gatekeeper" as a serious insult.

In {{ sevent(id="se002") }} after An calling Kanade zero-dimensional tulpa and implying I can't talk about headpats, I prove her that I can by offering her a headpat by myself. Both sides were mean to each other here.

Those are cases of mutual harassment at worst (for us). Also, in both cases An has been already talking about headpats to us.
"""
falsifiability = """
Find events of us genuinely offering An headpats after {{ sevent(id="se003") }} or mentioning headpats related to her after {{ event(id="e007") }} unprovoked by An being mean to us and talking to us about headpats already.
"""

[C003]
title = "An has been mentioning headpats on her own initiative"
description = """
Besides events mentioned in {{ conc(id="C002") }}, An has shown ability to talk about headpats multiple times. She has even given a headpat to Kanade in {{ sevent(id="se004") }}. Even a few hours before the WormHugs&trade; drama, An has been talking about headpats on her own initiative. {{ event(id="e011") }}
"""
verifiability = """
Events: {{ event(id="e007") }}, {{ sevent(id="se002") }}, {{ sevent(id="se004") }}, {{ event(id="e011") }} prove it directly
"""

[C004]
title = """An's boundaries didn't involve talking to her about headpats in general"""
description = """
An's asked us in {{ event(id="e007") }} to not associate headpats with her. It doesn't involve not talking to An about headpats at all.

Even if she said explicitly to not talk to her about headpats at all, she's would have been violating her own boundaries multiple times as concluded in {{ conc(id="C003") }}. It's arguable if such a boundary is tenable even without it.
"""
verifiability = """
In {{ sevent(id="se003") }} An has drawn her boundaries for a first time. Since then genuinely offering her headpats is not okay as she feels bad with refusing.

In {{ event(id="e007") }} An has drawn stricter boundaries -- to not associate headpats with her. Talking to her about (not) giving her headpats is not okay since then.

And even if you found An saying to never talk to her about headpats, such a boundary is definitely untenable as she's been talking about headpats by herself as concluded in {{ conc(id="C003") }}.
"""

[C005]
title = "Kanade has been consistently adapting her behavior to An's feedback"
verifiability = """
After {{ sevent(id="se003") }} Kanade hasn't given or offered An any headpats.

After {{ event(id="e007") }} Kanade has stopped teasing An with (not) giving her headpats.

After {{ sevent(id="se004") }} Kanade hasn't assumed that it was okay to give An a headpat after suddenly receiving one from her.
"""

[C006]
title = "We haven't abused An with offering her headpats. At worst (for us) there was mutual harassment."
description = """
As concluded in {{ conc(id="C001") }}, all discord headpats An has actually received, happened before any negative feedback from her.

As concluded in {{ conc(id="C002") }}, when we overstepped An's boundaries with offering her headpats, it happened after her being mean to us. It's mutual harassment at worst for us.

As concluded in {{ conc(id="C004") }}, just talking to An about headpats didn't violate her boundaries.

As concluded in {{ conc(id="C005") }}, Kanade has been adapting her behavior to An's complains.

There is no basis for our interactions with An related to headpats, to be abusive from our side.
"""
verifiability = """
Straight from {{ conc(id="C001") }}, {{ conc(id="C002") }}, {{ conc(id="C004") }}, {{ conc(id="C005") }}.
"""

[C007]
title = "An has lied about a particular instance of us supposedly leaking her DMs. There is no evidence on us leaking her DMs in general."
description = """
There have been two cases of An accusing us of that. 

First was the case of An's nationality described in {{ sevent(id="se001") }}. This case not included in Ranger's report is a little bit more complicated as it's arguable as she was talking about Poland in a server in a way that Poles would ~~mis~~<wbr>take her for another Polish person, people in general probably not so much. Also, it's not like I knew her being Polish just from DMs, we used to interact in a Polish server in the past. 

Second case, from {{ event(id="e008") }} is very simple though. She was talking about her problems with trust in a server and after a few weeks, when I mentioned those problems, she accused me of leaking her DMs.
"""
verifiability = """
Case from {{ event(id="e008") }} is straightforward.
"""
falsifiability = """
Find a case of us really sharing something we must have got to know from DMs shared with An.
"""

[C008]
title = """We have never shamed An for threatening to draw boundaries, threatened to send unsolicited friend requests or to leak her DM content"""
description = """
Ranger's report includes screenshots from {{ event(id="e001") }}, {{ event(id="e005") }} and {{ event(id="e008") }} under such accusation. We haven't:
- shamed An for threatening to draw boundaries
- threatened to send unsolicited friend requests (We have sent a friend request in {{ event(id="e001") }} when we have already had An's other account in our friend list)
- threatened to leak her DM content

In those events and in general.
"""
falsifiability = """
Find actual instances of us doing these things.
"""

# [C009]
# title = "There were multiple instances of An projecting on Kanade"
# description = """
# In {{ sevent(id="se013") }} An keeps repeating that Kanade doesn't care about other people's feelings and that other people feel pressured by her, despite the fact that other people in the conversation seemed happy about exchanging hugs and headpats.

# In {{ event(id="e007") }} she does it again. She says "Again about her" when Kanade says that she feels hurt by An's accusations.

# In {{ event(id="e011") }} she said that we lack basic capabilities of getting someone's perspective while it was her who didn't seem to want to reach mutual understanding with us.
# """
# verifiability = """
# The conclusion is about suspected instances, not about those cases definitely being An projecting on other people. Multiple instances in {{ sevent(id="se013") }}, {{ event(id="e007") }}, {{ event(id="e011") }} show that suspicion is valid and also shows that we have been suspecting An of projecting during these events too. Whether those really are projections - it's hard to verify or falsify.
# """

[C009]
title = "An has been distracting us and shifting responsibility when things didn't go the way she wanted. Alexandra, shierudo and Ranger have been helping her with it."
description = """
In {{ sevent(id="se013") }} An first avoids Nao calling her out on being mean to Kanade. She starts being mean to Kanade again and paints her false narrative about Kanade not taking "no" for an answer for the first time. She continues projecting her own problems on other people who Kanade is interacting with and never gets punished for it. Cerys is helping her with distracting us.

In {{ event(id="e007") }} An repeats her false narrative which hurts Kanade again. After being called out by Aki, An continues bullying Kanade and playing a victim. This time she is helped by shierudo.

In {{ event(id="e008") }} An accuses us of leaking her DMs and after being called out on talking about the "leaked" thing before, she says that there have been more unspecified cases and conveniently runs away. Shierudo helps sustaining her narration.

In {{ sevent(id="se018") }} An has been falsely implying that another user outed himself as a lolicon despite his continuous objections. When I called An out on that, she kept pushing all responsibility on the user she has been defaming and on me for disrupting her "helping" him. Cerys helped her again with that.

In {{ event(id="e010") }} An has spilled her frustrations about how I don't act in a way she'd like me to and makes a drama. She clearly harassed us and never took responsibility for it. This time no one of her friends was there to point out how her being the victim, unfortunately.

A few days later, in {{ event(id="e011") }} An made a drama after being asked about bitcoin to WormHug&trade; conversion ratio. She left the server after Zen told her that her boundaries are untenable. She then comes back for a while only to share her outburst with us in non-interactive way.

Shortly after that, in {{ sevent(id="se005") }} Alexandra has been trying really hard to make An look like a victim. 

Finally, Ranger's {{ report() }} itself might have been the biggest thing in this category. **It's not just defamation but also a distraction from An's abusive behavior.** 
"""

[C010]
title = "An has been minimizing Kanade's feelings"
description = """

In {{ event(id="e007") }} An is minimizing Kanade's feelings when informed that she feels bad due to what An said about her.

In {{ event(id="e008") }} it's a random user whose needs are minimized by An.

In {{ event(id="e010") }} she calls us the most boring and most predictable conversationalist and say we are obsessed with a few very basic topics. She's also minimizing my tulpas calling them "tupperbox profiles". 

In {{ event(id="e011") }} when Kanade complains about her boundaries being violated by An, she's accusing us of twisting the problem. 

"""
verifiability = "Shown in events mentioned above"

[C011]
title = "An has been demeaning and insulting us"
description = """
In {{ event(id="e007") }} An calls Kanade a creep that can't take "no" for an answer.

In {{ event(id="e010") }} she calls us the most boring and most predictable conversationalist and calls my tulpas "tupperbox profiles".

In {{ event(id="e011") }} An has called us _fucking_ Mon and another user a motherfucker.

After Ranger's report, it continued.

In {{ sevent(id="e015") }} An has called me a harasser, abuser, predator, monster and psychopath.

"""
verifiability = "It's all in mentioned events"

[C012]
title = "An has been discrediting us and pushing on us responsibility for her own problems"
description = """

Accusing us of not being able to take "no" for an answer when it comes to headpats, happen consistently in {{ sevent(id="se013") }}, {{ event(id="e007") }} and {{ event(id="e011") }}. An repeats the same lie again and again until people start to believe there must be something true in it.

In {{ event(id="e011") }} An has accused us of ruining results of her therapy.

After Ranger's report:

In {{ sevent(id="se015") }} An says that she doesn't want to DM people because of me.

"""

[C013]
title = "An has been lying about us and rewriting history"
description = """
An is consistently lying about what people are saying and doing. There are multiple instances of her saying that people say things that they didn't say and did things they didn't do. 
"""
verifiability = """
Before Ranger's report:

In {{ sevent(id="se013") }} An is exaggerating saying that Kanade wasn't able to take "no" for an answer for a long time, despite the fact that Kanade stopped offering her headpats at least 3 weeks before.

In {{ event(id="e007") }} she repeats the same thing, saying she's been harassed for months. 

In {{ event(id="e011") }} she does the same as before.

In {{ event(id="e008") }} An accuses us of leaking her DMs and ignores us saying that she's already shared in the server what we've been talking about. We've included the proof on that.

In {{ sevent(id="se018") }} An kept implying that another user outed himself as a lolicon after he clearly denied that.

After Ranger's report:

In {{ sevent(id="se009") }} An has falsely implied that **I said that** Kanade cannot control her actions and **I said that** lack of control could excuse her.

In {{ sevent(id="se016") }} An lied about me arguing for nothing being wrong with being attracted to children and painted a false narrative about me sharing my sexual fantasies and fetishes in front of minor (I did talk about it indeed... in NSFW channel).

In {{ sevent(id="se015") }} An consistently tells people that I'm a sexual predator among other things.
"""

[C014]
title = "An has been playing a victim"
verifiability = """
There have been a few made up covert contracts I have breached according to An.

- Leaking information from our DMs.
  - We have never promised to keep An's nationality a secret and didn't know it just from DMs with her. {{ sevent(id="se001") }}
  - An had talked about her problems with trust in the server. When we mentioned it in the server a few weeks later, she acted like we leaked it from our DMs with her. {{ event(id="e008") }}
- Talking to her about headpats. 
  - She was acting like we have genuinely offered her headpats when we didn't. E.g. {{ event(id="e007") }}
  - She said that we shouldn't have talked to her about headpats at all in {{ event(id="e011") }} despite us never agreeing to it and her talking about headpats by herself.
"""

[C015]
title = "An was being consistently trigger by us, especially by Kanade's headpats, in a place she considered her own safe space."
description = """
An has been consistently triggered with us talking about topics uncomfortable for her. It probably included not just headpats but also talking about priests abusing children or sharing my internal relationship with some of my tulpas (who I consider both my lovers and my siblings).

Trying to manipulate me and how other people see me was her way of protecting herself from her triggers. She needed to get rid of a certain headpat-obsessed tulpa and her taboo-breaching system to continue feeling comfortable in the place she (as she admitted in {{ event(id="e011") }}) was using as a safe space.
"""
verifiability = """
An revealed being abused by her family members as a child and directly revealed mentioning headpats triggering her in {{ event(id="e011") }}. An also admitted to using tulpa.info as her safe space in {{ event(id="e011") }}.
"""

[C016]
title = "An's behavior towards me fulfills the definition of gaslighting while mine towards her doesn't"
description = """

Gaslighting consists of abusive behaviors like lying, discrediting, distracting, minimizing, shifting blame, denying wrongdoings, rewriting history etc. **consistently** over time **in order to manipulate the victim**.

An has demonstrated a wide range of abusive behaviors towards me, applied consistently, over long period of time, with a clear goal in mind.

I have demonstrated a few instances of harassment towards An at worst and had no reason to manipulate her.
"""
verifiability = """
There are few example of mutual harassment between me and An as in {{ conc(id="C002") }} on one side and a lot of abusive behaviors demonstrated by An ( {{ conc(id="C010") }}, {{ conc(id="C011") }}, {{ conc(id="C012") }}, {{ conc(id="C013") }}, {{ conc(id="C014") }}) on the other side. Furthermore, manipulation has a goal. Contrary to me, An has a motivation to manipulate me and how other people see me{{ conc(id="C015") }} -- getting rid of a person triggering her and ruining her safe space.
"""

[C017]
title = "An has been using other people to make her claims credible"
description = """
Other people have been helping An. Alexandra and shierudo have consistently supported her during the case. Especially Alexandra, who has been doing a lot of dirty work for her in the final drama.

Eventually, she started getting support of other people who bought her playing the victim, like Anubis and, most importantly, Ranger -- the author of a report defaming me.
"""
verifiability = """
Alexandra and Cerys have consistently pushed responsibility on us for An's antics, e.g. in {{ sevent(id="se013") }}, {{ sevent(id="se016") }},  {{ sevent(id="se020") }}.

After An left the server, they have been blaming us in {{ sevent(id="se005") }} and in {{ sevent(id="se017") }} Alexandra has revealed that they about false narratives in the community.

Shierudo's actions e.g. in {{ sevent(id="e008") }}, {{ sevent(id="se016") }}.

[Ranger's report itself](/report-archived.html#comment-374939).

People taking An's side in {{ event(id="e011") }} after she repeated her lies about us abusing her for months for n-th time.
"""

[C018]
title = "Evidence included in Ranger's report is inconsistent with its thesis. The report is saying things that aren't true."
description = """
Ranger claimed that we have shamed An for threatening to draw boundaries, threatened to send her unsolicited friend request and explicitly threatened to leak DM content and provided screenshots that show none of those acts for evidence. This is the most obvious part that can be seen even without any further context provided.

**[Analysis of the report](/report-with-context)** shows that the evidence included in there doesn't prove its accusations being true.

Proving the accusations being false requires more effort but we did it with previous conclusions:
- Kanade has been consistently adapting her behavior when being told to stop offering headpats to An and teasing her with them. {{ conc(id="C001") }}, {{ conc(id="C002") }}, {{ conc(id="C004") }}
- Kanade's mentions of headpats _at worst_ contributed to **mutual** harassment between us and An. {{ conc(id="C006") }}
- Contrary to An's accusations, we haven't leaked her DMs. She has shared the stuff we supposedly leaked in the server before. {{ conc(id="C007") }}
- Also, we haven't committed acts of boundary violation unrelated to headpats. {{ conc(id="C008") }}
- An's actions towards us had significantly more likeness to gaslighting than our actions towards her. {{ conc(id="C016") }}
"""
verifiability = "Derived from mentioned conclusions combined"

[C019]
title = "Ranger has made the report behind our back and refused to talk about it after we reached her"
description = """
I wasn't notified about the report before or after its publication. I found it out by myself after checking out tulpa.info forums.

When we reached out to Ranger about it, we got immediately blocked and us calling her out on obvious inconsistencies in the report was ignored. 
"""
verifiability = """
I've published our DM content showing Ranger's attitude. {{ sevent(id="se012") }}
"""
falsifiability = """
Show me how I was notified about the report before trying to contact Ranger by myself.
"""

[C020]
title = "Ranger is the author of the whole content of her report and she made the report on her own initiative"
description = """
An stated that didn't know about the report before coming back to the server. There is no proof of other people being involved in making it. Therefore Ranger is fully responsible for lies she's been saying about me.
"""
falsifiability = """
Prove other people's impact on content of Ranger's report or their initiative in making it. To be honest, I have some suspicion here I had expressed in one of my previous documents but no proof of it.
"""

[C021]
title = "Ranger's report was aimed to have consequences for me outside tulpa.info and had such consequences in practice"
verifiability = """
Ranger has admitted to reporting me to Discord Safety and Trust Team. It proves that report was aimed to get me banned on whole discord.

I have been preemptively banned in another server due to its mods trusting Ranger's report. It proves that report indeed had real consequences for me outside tulpa.info.

Both cases are included in {{ sevent(id="se006") }}.
"""

[C022]
title = "Ranger's report is a purposeful act of defamation against me"
verifiability = """
Ranger had personal motivation for making a report as she felt personally responsible for me being able to _abuse_ An for months, as she said in {{ sevent(id="se009") }} 

Ranger report has said many bad things about me that aren't true as concluded in {{ conc(id="C018") }}.

Ranger made the report by herself and on her own initiative {{ conc(id="C020") }}, admitted to have intention to use it against me outside of tulpa.info by reporting me to discord and the report had real consequences for me in another server. {{ conc(id="C021") }}

Ranger refused to correct any kind of mistakes in her report {{ conc(id="C019") }}.
"""

[C023]
title = "After the report had been published, An kept defaming us"
description = """
In {{ sevent(id="se016") }} An accused us of:
- clinging to her like a glue in tulpa.info before I got banned (no comment on that)
- talking about sexual fantasies and fetishes in front of minors (she didn't mention that it took place in nsfw channel and I'm not responsible for minors lying about their age to both discord and mods)
- saying there is nothing wrong in being attracted to children (which we didn't say)
- playing victim (which was what she is doing as concluded in {{ conc(id="C014") }} )

In {{ sevent(id="se015") }} she called me a _predator_ without specifying anything I've done to deserve such a title. She also called me an abuser/harasser, implied that I might be a pedophile and was talking about torturing people like me being OK.
"""
verifiability = "As in linked evidence"

[C024]
title = "Pleeb has been consistently covering Ranger despite the fact that information about her report containing obvious lies reached him"
verifiability = """
Pleeb (and also Reguile) admitted to have read my first counter-report and stated that there are no substantial arguments there. He's also received my second counter-report, I have no evidence on him having read it or not though. {{ sevent(id="se009") }}

Ranger is still member of tulpa.info staff, currently a chat mod, despite the fact that admins know about her actions being aimed to defame a user. You can find many instances of Pleeb praising Ranger.
"""

[C025]
title = "Multiple members of the tulpa.info staff (including its owner) are responsible for an act of defamation against me"
description = """
This whole thing is not the case of misunderstanding or a rouge mod abusing power under administration's nose. Admins covered Ranger after being provided evidence on her wrongdoings and kept praising her. {{ conc(id="C024") }}

It's a case of multiple members of the staff consistently and purposely participating in such an abuse of power and trust. Tulpa.info is responsible for an act of defamation against me and it can't be pushed on Ranger alone or blamed on An manipulating the staff. Ranger has made the report on her own initiative in purpose to defame me {{ conc(id="C022") }} and Pleeb has covered her despite knowing about the report's lies {{ conc(id="C024") }}. Pushing blame on An won't help Ranger and pushing it on Ranger won't help Pleeb avoid responsibility at this point.
"""
verifiability = """
Conclusions mentioned above lead to this one.
"""

[C026]
title = "Tulpa.info's moderation system is executed depending on staff's mood rather then if rules were broken or not"
verifiability = """
Tulpa info has (had?) strike (basically, official warning) system. 

I've got some strikes both as Mon and Felix:
- I've got 2 retroactive strikes after the WormHugs&trade; drama, based on An's (false, as {{ conc(id="C016") }} and dependent prove) accusation. Furthermore, I've been later banned (and defamed with Ranger's report) for the same thing, after a few months. Basically, I've been punished for the same thing (that I didn't do btw.) twice.
- When we visited tulpa.info incognito, Sora has been saying that she and I are both siblings and lovers. Yeah, we got a strike for specifically that. I don't recall imaginary incest being against any rules.

On the other hand:
- I haven't got any strikes for bullying and trolling metaphysical folks in {{ sevent(id="se010") }} and {{ sevent(id="se011") }}.
- An hasn't got any strikes for harassing Matthias in {{ sevent(id="se018") }}.
- Anubis hasn't got any strikes for harassing me in {{ sevent(id="se008") }}. On the other hand Pleeb has told me to shut up when I called him out on that.
"""