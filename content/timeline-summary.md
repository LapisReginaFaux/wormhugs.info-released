+++
title = "Timeline Summary"
draft = true
+++

#### 2021-08-22 - 2021-08-26

It's {{ event(id="e001") }}. A time before An has given Kanade any feedback on her headpat-related stuff.

Kanade gives An a discord headpat and a discord hug.

She also sends a friend request to KitKat's account when we already have Leiko's account in our friend list.

After An gives Kanade a hint about consent, Kanade takes it and asks An if she'd like a hug before (not) giving it to her.

#### 2021-08-28

It's {{ event(id="e003") }}. Kanade is offering An a headpat/hug again and abstains after the offer is declined. A few jokes about headpats being illegal happen.

#### 2021-08-29

An says that in her point of view, a headpat is a patronizing gesture and that imposing it on others is not okay and offering people headpats put pressure on them. Kanade apologizes for being imposing.

Since this point in time, offering hugs/headpats to An should be considered rude.

#### 2021-09-07

Kanade and An are joking together with other people. After An said that it's illegal for Kanade to be 1 month old, Kanade said that she is illegal as she headpats people without consent and that An is her favorite subject. After people saying that Kanade learns today what "no" means, she asks if it means "I secretly want your headpat". Meanwhile An said that Kanade's headpats made her feel violated and disrespected.

Shortly after that, Kanade asks An about other ways she can show affection to her.