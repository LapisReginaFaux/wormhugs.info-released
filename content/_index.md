+++
title = "WormHugs.info -- exposing abusive and defamatory practices of Tulpa.info"
+++

{% caution(header="This website is targeted for tulpamancers only") %}
The case might be hard to understand for people having no experience with tulpas and explaining them is outside of its scope. I'm not going to endorse any specific sources here as it's not my intention to favor any of tulpa.info's competitors.
{% end %}

---

#### Update 2024-03-18

I decided to read it once again a year later, after gaining some distance to it. Except fixing a few typos, I can't see anything significant to update in the content, it will probably stay intact for as long as I decide to pay for the domain and keep this website at wormhugs.info. 

On 2023-09-21 Pleeb has made an announcement about Tulpa.info discord server shutting down. Today it's still read only and I hope it stays that way. While I don't think this website or my actions in general had decisive agency in that, one of its main purposes was achieved.

It seems that original links to pictures in Ranger's defamatory report have expired. A copy in [web archive](https://web.archive.org/web/20221016204149/https://community.tulpa.info/topic/1428-moderator-reports/page/13/) still includes them.

---

### What is it about?

I'm Mon, a tulpamancer. Also, I'm not a native English speaker, so please forgive me minor mistakes.

**At the turn of the year 2021/2022 we have fallen a victim of an abuse in tulpa.info discord server.** I'm going to call our abuser An throughout the website as it's her most recent nickname

One of my tulpas, Kanade, is really obsessed with headpats and _worm_ hugs. **An have been repeatedly sabotaging her, painting a false narrative about Kanade not being able to take "no" for an answer** despite the fact that Kanade's:
- immediately stopped _headpatting_ An (in italics in discord chat) and offering her such headpats when asked to not do it,
- immediately stopped talking about headpats related to An when asked to not do it.

**An has also been falsely accusing us of leaking stuff from her DMs** despite her sharing those stuff in the server before we mentioned them too. 

Except lying, rewriting history and playing victim, An also has been repeatedly:
- **insulting us in the chat**,
- **minimizing Kanade's feelings**,
- **projecting her own problems on Kanade and other people**,
- **distracting us and denying any wrongdoings when things didn't go her way**.

An had at least two accomplices in her abuse, shierudo and Alexandra/Cerys (they are tulpas in one system, which as a whole was sometimes called Simulacra). They've consistently supported An's false narrations and helped make them look credible for other people too. **Simulacra also pretended being our friends and have been manipulating us into blaming ourselves for An's antics, distracting us from An's malicious intentions and questioning our general behavior being socially acceptable.**

On 2022-03-24, An leaves tulpa.info after a drama that started with Kanade asking about BTC to WormHug&trade; conversion ratio as a joke. Before leaving, An has admitted to having PTSD triggers related to headpats and accuses us of ruining results of her therapy. Tulpa.info's owner, Pleeb, has taken An's side and I've been blamed for this, ended up receiving two retroactive warning for things An has accused me of. **Still under influence of An's and Simulacra's gaslighting, we weren't able to defend ourselves properly and decided to accept it as it is.**

**Suddenly, after a few months, on 2022-08-15, in [tulpa.info moderation log](https://community.tulpa.info/topic/1428-moderator-reports/?do=findComment&comment=374939), there has been published a report [(link to copy)](/report-archived.html#comment-374939) about how I have "targeted another member for months through non-consensual roleplay touching, other emotional and cyber abuse, and gaslighting".** I've also been banned from the server, without any warning, I wasn't even notified about it (the report publication) and got to know about it when I noticed tulpa.info missing in my server list a few days after it being published and checked the forum. **After confronting Ranger in DMs about some obvious mistakes (as at that point I wasn't convinced about her malicious intentions), I've been immediately blocked.**

**Pleeb and the other admin, Reguile, have also ignored evidence of Ranger's report lying about me and they kept supporting Ranger being aware of these lies.**

Tulpa.info staff not only **failed to identify abuser and victim in the case** but **defamed the real victim**, making the psychological and social impact of the abuse a magnitude higher. Furthermore, **they refused to acknowledge any of their mistakes despite being provided hard evidence on it.**

Also, **their actions encouraged An to defame me even further than Ranger's report did**. Recently, she's even started telling people that I'm a pedophile and a sexual predator, among other things and still doesn't face any consequences for that.

### Goals of WormHugs.info:
- Making An take responsibility for her abusive behavior by exposing it publicly
- Exposing Ranger's malicious intentions and defamatory nature of her report.
- Leading Tulpa.info to lose the trust they have in general tulpa community by exposing their defamatory practices and covering it.

### Non-goals:
- Complaining on being banned in tulpa.info -- Ban is a valid action to get rid of someone you don't want to have in your server. It doesn't excuse an act of defamation though.
- Getting the report taken down silently -- It's far too late for that to undo its consequences for me. If they do it, I'll consider it nothing more than trying to cover things up.
- Leading to Tulpa.info push all the responsibility on Ranger -- Not gonna leave a way out for Pleeb. He consistently covered Ranger despite being aware of the evidence of her report containing obvious lies.
- Playing the victim -- unfortunately, there is a lot of evidence of me being a victim of An and by extension, Ranger defaming me as an abuser in that case. 

### Layers of content
To make it less chaotic and easier to grasp than previous documents, this time I've put content of the website into layers:
- [Evidence](/events) consisting mostly of screenshots with commentary, documenting:
    - Events mentioned first in Ranger's report, provided with additional context 
    - Ranger's report itself with our commentary
    - Other events I have found significant in this case
- Semi-formal [Conclusions](/conclusions) derived from linking together evidence and previous conclusions.
- High level [Summary](/summary) of the case, assembled from all of the conclusions

I recommend starting from [summary](/summary) to get to know my whole point of view and reaching deeper layers when you want to see proofs on my claims. You're also welcome to read the [evidence](/events) in chronological order if you have time.

---

If you'd like to talk with us about the case, you can join a [Discord server](https://discord.gg/4tPDh4JZUV) dedicated for that. Feel free to DM us too. Major changes to the website since its release will be mentioned in [changelog](/changelog) and all changes can be traced in our public [Gitlab repo](https://gitlab.com/LapisReginaFaux/wormhugs.info-released).