+++
title = "Summary"
+++

The first big conclusion is that our interactions with An related with headpats weren't abusive: {{ conc(id="C006") }}
- All actual discord headpats and genuine headpat offers An has ever received from us happened in a span of one week, before An told us that she doesn't want headpats and she feels bad with declining unwanted offers. {{ conc(id="C001") }}
- After being told to not associate An with headpats at all, we have also accepted her wish. In general, we always listened to her feedback. {{ conc(id="C005") }}
- There were at most 2 instances of mutual harassment between An and us when we were being mean to her and gave her unsolicited headpat offers. {{ conc(id="C002") }}
- An has been mentioning headpats on her own initiative too {{ conc(id="C003") }} and never said that us just talking to her about headpats unrelated to her are bothering her. {{ conc(id="C004") }}

We also haven't violated An's boundaries in other ways Ranger's report accuses us of:
- Contrary to An's accusations, we haven't violated her DMs. We've proven that she has publicly shared the information we supposedly leaked a few weeks before we did. {{ conc(id="C007") }}
- There is no evidence for us ever shaming An for threatening to draw boundaries and threatening to leak her DMs. {{ conc(id="C008") }}
- We have indeed sent An a friend request (not threatened to send it like Ranger said it, lol). Since when it's a crime? {{ conc(id="C008") }}

Out of the two of us, it's An who shown plenty of abusive behavior. Those are behaviors that gaslighting consists of: {{ conc(id="C016") }}
- She's consistently minimized Kanade's feelings and exaggerated her wrongdoings against her. {{ conc(id="C010") }}
- She's consistently been demeaning and insulting us. {{ conc(id="C011") }}
- She's consistently discredited us and was pushing on us responsibility for her failures {{ conc(id="C012") }}
- She's been lying and rewriting history. A lot. {{ conc(id="C013") }}
- She's been manipulating us and how people see us by playing a victim {{ conc(id="C014") }}
- She's been using help of other people to make her claim look more credible. She had two friends who consistently helped her from the start and at the end some people started to believe that there must be something behind lies repeated n times already. {{ conc(id="C017") }}

From a certain point of view, An was trying to protect herself and be able to feel comfortable in a place she used as a safe space again. It was motivation of her manipulations. An suffered from triggers she contracted from abuse she faced in childhood. And I often stepped on those triggers, in particular with Kanade's obsession with headpats. {{ conc(id="C015") }}

Ranger has committed a purposeful act of defamation against me: {{ conc(id="C022") }}
- She felt personally responsible for supposed abuse An faced from me. {{ conc(id="C022") }}
- She made a report saying many bad things about me with evidence disconnected from its thesis. {{ conc(id="C018") }}
- There is no evidence for other actors being directly involved in making the report. {{ conc(id="C020") }}
- The report was designed to have real consequences for me outside tulpa.info and it indeed had. {{ conc(id="C021") }}
- The report has been made without asking me for my point of view and I wasn't notified about it before and even after its publishing {{ conc(id="C019") }}
- Ranger refused to talk about the report and correct even most obvious mistakes we pointed out to her. {{ conc(id="C019") }}

Encouraged with Ranger's act of defamation, An pushed it further. Recently, she's even started painting me as a pedophile and sexual predator. {{ conc(id="C023") }}

Owner of tulpa.info, Pleeb, has been aware of evidence of obvious lies coming from Ranger's report. {{ conc(id="C024") }} He kept covering and praising her, Ranger still is a moderator in tulpa.info. Another admin, Reguile, has also admitted to read the evidence of Ranger's wrongdoings and ignored it. So in the end Ranger's report isn't an action of a rouge mod under administration's nose. It's responsibility of whole tulpa.info staff. {{ conc(id="C025") }}

During the case, there were found multiple cases of staff giving official warnings for stuff that didn't break any particular rules but ignored behaviors that have clearly broken rules. {{ conc(id="C026") }}

## Expectations

At this point I have no expectations from people involved in abuse I faced. From my point of view, it's too late for apologize and if these people will regret anything, they probably going to regret getting exposed. 

Instead, I'd like others to know what these people are capable of:
- I'd like you to know what kind of self-righteous defamer hides behind the apparently kind person that Ranger portraits herself in public. A hippo fits Ranger -- despite looking deceptively docile, they are the most dangerous animals for humans in the savannah.
- I'd like you to know what kind of people staff of tulpa.info consists of, in particular Pleeb and Reguile. They kept covering abuse in their ranks after being provided evidence of that. Like in Catholic Church but they don't even move abusers to different parishes.
- I'd like you to know how dangerous An is. If you read the content of this website, you've been shown ultimate display of abusive behavior she is capable of. If she ever sees danger in you, you might be her next target.

It's up to you to decide what to do with that knowledge. 

## What's outside the scope of WormHugs.info -- tulpa.info and tulpamancy

I think the website has enough content without debating whether tulpa.info is a good place for people learning tulpamancy. I decided that judging tulpa.info as tulpamancy learning resource is beyond the scope of it, at least for now.

I'll say only one thing in this topic. Whether it's good or not, there are plenty other places that are just as good or better in that aspect. I won't endorse particular ones here but I encourage you to check out other servers too. You can join a few other servers on the top of [Tulpa tag in Disboard](https://disboard.org/servers/tag/tulpa) and try them out. 