+++
title = "Ranger's report"
date = "2022-08-15"
[extra]
report = true
+++

On **2022-08-15** Ranger has published a report in tulpa.info's moderation log. You can access it [here](https://community.tulpa.info/topic/1428-moderator-reports/?do=findComment&comment=374939) and [here](/report-archived.html#comment-374939) is a saved copy in case it's removed.

## Abstract

{{ rimage(ids="report-abstract" ) }}

This section is redundant with others when it comes to accusations so I'm going to address them later, in specific sections.

## The abusive behavior

{{ rimage(ids="report-abusive") }}

First part of actual report’s content claims that main form of our abusive behavior was non-consensual roleplay touching. We had repeatedly directed, mentioned, tagged, brought up headpats etc. around An. A collection of screenshots is posted as an evidence. All of those screenshots are explained in further context in the events part of the website.

### Headpats

{{ rimage(ids="report-headpats") }}

In this subsection, it's said that the main form of our _abuse_ was _non-consensual roleplay touching_. We are supposed to have repeatedly directed, mentioned, pinged with, bring up _roleplay touching_ around An. A collection of screenshots below is supposed to prove it.

{{ rimage(ids="soup") }}

Before addressing the evidence, let's address the accusations first. It's certainly true that we (Kanade in particular) have been talking about headpats a lot. What isn't true is accusation of Kanade doing stuff related to headpats in a way that violated boundaries stated by An. She's drawn such boundaries twice:
- In {{ sevent(id="se003") }} An said that she feels bad with receiving headpat offers as she doesn't want headpats and feels bad with refusing such offers
- In {{ event(id="e007") }} An told us to not associate headpats with her at all.

Breaching boundaries drawn by An after they are drawn is a harassment. Doing it before they are drawn isn't. And so isn't breaching imaginary boundaries An has never drawn. She clearly (eventually) said in {{ event(id="e011") }} that she doesn't mind people talking about headpats. She's also been talking about headpats at her own initiative, even giving it to Kanade once in {{ sevent(id="se004") }}.

To sum up each event included in this collection, let’s at least group them by date:

##### 2021-08-23 - 2021-08-26

{{ rimage(ids="r001_3,r001_4|r001_5") }}

Those pats and and a hug offer happened in {{ event(id="e001") }}, before An has drawn any boundaries. So it can't count as a harassment. Btw. the marked part of second screenshot refers to Alexandra, not An.

##### 2021-09-07

{{ rimage(ids="r004_3,r004_5") }}

It's all from {{ event(id="e004") }}. In the first screenshot Kanade asks An how can she show her affection without headpats. 

In the second screenshots, the top two messages are pasted in reverse order. It suggests Kanade assuming that An dislike physical touch and then textually poking An. In reality, Kanade poked An to get her attention earlier and Kanade asked her question about An disliking physical touch in context of the _5 languages of love_ An have been talking about.

In bottom question Kanade bluntly asks An if she should pretend to distance herself from An or just be persistent if she wants to be closer to her.

Keep in mind that events from these screenshot happen after An has told us that she feels bad with having to reject unwanted headpat offers. There is no such an offer here.

##### 2021-09-20 - 2021-09-22

{{ rimage(ids="r005_2,r005_3") }}

It comes from {{ event(id="e005") }}. In the first one Kanade tells An that cat videos are as valuable for Kanade as headpats are for An. In the second one, she asks Alexandra (not An) if she liked headpats in her previous incarnation we discussed as a joke.

No headpat offers here either.

##### 2021-10-05

{{ rimage(ids="r006_1,r006_2") }}

In the first screenshot An asked Kanade how much she's going to pay Alexandra for her services as a professional detective. Kanade interprets it as An offering her own services instead and, as she's going to pay in headpats (obviously), she's refusing An and doesn't offer her headpats just as she wanted.

In the second screenshot I encourage An to saying _hug_ in italics with a star wars meme.

No headpat offers he as well.

##### 2021-10-13

{{ rimage(ids="r007_1,r007_2") }}

It's {{ event(id="e007") }}

In the first screenshot, in the marked Kanade is asked by An to let me headpat another user. Kanade refused and told An that she can ask me for a headpat though if she wants, since Kanade won't offer her headpats.

Second screenshot comes just after the first. Kanade says that perverts finding headpats sexual (it was referring to shierudo's question earlier in {{ event(id="e007") }}) and after that, An calls her a creep and implies that Kanade harassed her for headpats for months and wasn't able to take _no_ for an answer. That implication is not supported by any of earlier screenshots (and, I daresay, it's not supported by any kind of evidence covered by the report or not).

Just after those screenshots, An has drawn stricter boundaries. This time about not associating headpats with her at all. About her previous boundaries -- I can see a world where An interprets what Kanade told her about asking me for headpats as a borderline offer. About as much as calling Kanade a gatekeeper being seen as a borderline insult. In the worst case it was mutual harassment from both of us.

##### 2022-03-20 (Almost half a year later)

{{ rimage(ids="r010_1") }}

In {{ event(id="e010") }} Kanade asks An if a tulpa An has just made up likes headpats. It isn't referring to any headpats regarding An.

##### 2020-03-24

{{ rimage(ids="r011_1,r011_2,r011_3") }}

These screenshots come from a drama that led An to leaving the server for about half a year and have become inspiration for this website's name. An joked about another user having to pay 500 BTC penalty and Kanade asked how much is it in WormHugs&trade;. An got triggered and asked how many times she has to repeat unspecified _it_ for Kanade to understand. Kanade asks An what her problem really is and says that from our point of view, we aren't doing anything wrong.

After not getting reply for some time, Kanade asks again to explain what An's problem really is just once since from our point of view, she isn't clear about it with us at all.

In the third screenshot Kanade specified that she had in mind what's the 500 BTC value in hugs is and wasn't offering An hugs or referring to headpats related to An. She didn't even use discord reply function, just referred to something coming from An's message, not even counting for An to be the one who answers her question.

Kanade said it explicitly in the screenshots -- she neither offered An hugs nor referred to hugs related to An. So she didn't violate boundaries related to hugs/headpats An has drawn previously.

---

To sum up, according to the events covered by the  report alone, we could argue that there is no evidence of headpat-related harassment from us to An. In the case from {{ event(id="e007") }} one could interpret Kanade's words to An as offering her headpats, even if it's really farfetched. We could also say that An has insulted Kanade before if we want to assume bad intentions at both sides rather than good intentions at both sides.

To be fair, there is a one whole instance of real harassment related to headpats I (not Kanade) have committed. In {{ sevent(id="se002") }} I've offered An a headpat after she's portrait Kanade as a _zero-dimensional_ tulpa. Here harassment at both sides is evident.

Anyway, I'm going to conclude with that:

There is no evidence for us violating An's boundaries in ways related to headpats, in Ranger's report, except for at most 2 cases of mutual harassment between us.

### Asking to stop

{{ rimage(ids="report-stop") }}

Ranger says that we have been told publicly to stop by An, other people and staff members. Both publicly and privately. And despite this, out behavior didn’t change. 

Screenshots are provided again, let's analyze them one by one.

{{ rimage(ids="stop1") }}

This is actually the group of screenshots:
- The first one happens during {{ event(id="e003") }}. Kanade asked An if she'd like a headpat, she didn't. Kanade, not being given consent, didn't headpat An, as you can see in the whole {{ event(id="e003") }}.
- The second one comes from the same event. An joked that hugs are illegal in 2021 and Kanade replied that it makes them feel even better. I'm not sure what's it supposed to show. Kanade simply replied to a joke with a joke.
- The third one coming from {{ event(id="e007") }} I've accidentally already covered in previous section. It actually comes here, not in the previous collection of screenshots. An's implication about Kanade harassing her with headpats for more is not consistent with any kind of evidence provided by Ranger's report (or by me in the website here later). Furthermore, it doesn't show An drawing stricter boundaries that happens in this event just after that.
- In the fourth one from {{ event(id="e011") }}, An calls Kanade's question harassment. Kanade specified shortly after that she her question wasn't directing those worm hugs at An and she simply asked how much is a BTC worth in hugs.

None of these screenshots proves Ranger's accusation. The two last of them show An's feelings but it only proxies her own accusations and doesn't prove them.

{{ rimage(ids="stop2") }}

This screenshot is taken out of context. In {{ event(id="e004") }} people are joking around and so is Kanade. When An says that Kanade's headpats made her feel violated and disrespected, we don't notice she was talking seriously and Kanade makes another joke. Kanade has actually never offered KitKat a headpat from her after {{ sevent(id="se003") }} which happened before this joke.

{{ rimage(ids="stop3") }}

This is also 2 screenshots grouped into one.
- In the first one is Luna who asks An to stop manipulating us after An admitted to purposefully trying to make us feel angry in {{ event(id="e010") }}. Az then _reminds_ us (I haven't found any evidence of us being told that before) to not tag An with headpat related stuff. In {{ event(id="e011") }} Kanade asks her questions about 500 BTC from An's message without tagging her... So I guess we stopped when Az asked us to.
- The second one is Az's summary after a drama in {{ event(id="e011") }} wrapped up. As with screenshots from previous group, it doesn't show evidence of Ranger's accusation. It just shows another person accusing us of the same.

---

I'll come back to Az's summary later, in other context.

But about Ranger's accusation of not stopping after being asked to, the report doesn't prove that. The linked _evidence_ simply shows that other people have been accusing us the same. It doesn't show an event of Kanade (or us in general) being told to stop and not listening. 

And previous section shows that we were consistently respecting boundaries drawn by An, regarding headpats stuff. As I said there, after {{ sevent(id="se003") }} we haven't offered An headpats and after {{ event(id="e007") }} we've never talked about headpats related to An. Well, okay, we did it once, on purpose, in {{ sevent(id="e002") }} in reaction to An harassing us first. If we really want to assume both sides bad will, there is also a borderline offer after borderline insult in {{ event(id="e007") }}, just before An has drawn stricter boundaries.

Ranger claimed that staff members asked me privately too to stop. I remember Ranger doing it twice. And I have to admit. Once, just after {{ event(id="e007") }} she repeated what An told me. And I know there was one more instance, unfortunately I lost access to it after deleting my account and I don't remember what Ranger exactly told me there. What I remember though is that **I didn't care about what Ranger was telling me back then.** So yeah, I admit to ignore what she's been telling me in DM whatever it was. **I've never ignored An's feedback** though and I think it's what matters here.

### Boundary violation

{{ rimage(ids="report-boundaries") }}

Accusations here are pretty straightforward. I've supposedly:
- Shamed An for having boundaries
- Threatened to send unsolicited friend requests
- Was called out by An that I've violated her DMs
- Explicitly threatened to leak DM content

If only the evidence was as straightforward.

{{ rimage(ids="boundaries1,boundaries2") }}

It's one screenshot split into two parts. It's {{ event(id="e001") }}, before An has drawn any boundaries related to headpats, so don't be scared by Kanade headpatting her here.

Kanade's also sent An a friend request from my account. I guess this is what Ranger's referred to as _threatening to send unsolicited friend request_. Well, we simply sent it, didn't threaten to send it and... what's exactly wrong with sending a friend request to another person? Btw. we've had her other accounts in our friend list back then...

{{ rimage(ids="boundaries3") }}

This screenshot comes from {{ event(id="e004") }}. Kanade revealed the fact that An has blocked DMs by default. It's a fact you get to know about if you try to DM someone, not someone you're entrusted with. I don't if it's supposed example of me violating An's DMs... If it is, it's ridiculous and I have no further comment.

{{ rimage(ids="boundaries4,boundaries5") }}

It's from {{ event(id="e008") }}. An accused us of leaking her DM content after we mentioned her problems with trust. Aki insisted that An's been talking about those problems in the server too. We have found a proof on that, I'll paste it here too:

{{ uimage(ids="u008_6") }}

So it turns out that we haven't leaked An's DMs here and she's falsely accused us of it. An said here that we've leaked other things in the past but she's never specified those things. We have found one thing that is as borderline as offering An headpats in {{ event(id="e007") }}. In {{ sevent(id="se001") }} we show the case of An's nationality being borderline revealed twice. First by An, then by me. In this case it's also worth noting that it's not like her nationality is something I know just from our DMs. We have interacted in a Polish server before meeting again in .info.

---

So, about accusations from this section:
- There is no evidence of us shaming An for having boundaries
- We have sent (not threatened to send) a friend request to a person whose other account was already in our friend list
- We've been falsely accused by An of leaking her DM content as we've proven that she shared publicly what she accused us of leaking first
- There is no evidence of us threatening An to leak her DM content

I dare to say, this section of Ranger's report is probably the most ridiculous. You can see evidence disconnected from its claim for most of the accusation even without any further context. Still, Ranger and people covering her (namely Pleeb and Reguile) could see nothing wrong even here.

### Gaslighting

{{ rimage(ids="report-gaslighting") }}

Here we are accused of gaslighting and by definition:
- lying, deceiving, rewriting history, denying our wrongdoings, shifting blame
- discrediting, minimizing, saying nice things with ill intent

{{ rimage(ids="report-gaslighting2") }}


Below the screenshots, Ranger specified a few concrete accusations of us:
- Lying about not giving target headpats
- Lying about our intentions
- Putting forward a false narrative about An being the abuser
- Denying any wrongdoings when being called out for abusive behavior
- Saying that we like An after insulting/demeaning her

Let's analyze screenshots one by one:

{{ rimage(ids="gaslighting1,gaslighting2") }}

Here I asked An if she lived in the previous universe. She says that she might be involved in creation of this one. We ask her if she is responsible for this one sucking so much, she says that she won't tell so she doesn't get the blame. We accuse her of escaping responsibility. A random bystander asks An to fix the timeline. Kanade tells them that An probably can't do that anymore since it always makes things worse in the movies. Then she says that she still likes An even if she was responsible for fucking up the timeline.

This is a fragment of roleplay from {{ event(id="e005") }}, initiated by An. Obviously, it doesn't fit any of Ranger's accusation but if I have to shoot I think it was supposed to be the evidence on us saying that we like An after demeaning her. There are no documented instances of us ever insulting An before or during {{ event(id="e005") }}. And it's not like we have been mean to her a lot after that.

{{ rimage(ids="gaslighting3") }}

Here Luna and An mutually insult each other during the drama from {{ event(id="e010") }}.

{{ rimage(ids="gaslighting4") }}

Still {{ event(id="e010") }}. Here Luna tries to see good intentions in An’s actions before but still feels manipulated by her and asks An to stop playing with our feelings. Which An has admitted to, it’s not putting any narration but calling her out on that.

After Az asking us to not tag An with headpat-related stuff, Kanade says that she is not offering her headpats anymore. And she (as Kanade) isn't since {{ sevent(id="se003") }}. And as all of us too, except {{ sevent(id="se002") }} where it happens as a part of mutual harassment between us and An and maybe {{ event(id="e007") }} if you really want to assume worst intentions of both actors.

So I guess it was supposed to be an evidence of Kanade _lying about not giving An headpats_.

{{ rimage(ids="gaslighting5") }}

It happens during {{ event(id="e011") }}. Luna accuses An of projecting her problems on Kanade in response to An telling Kanade that she lacks empathy and cognitive capabilities. Another mutual insulting.

{{ rimage(ids="gaslighting6") }}

Here I'm trying to take partial responsibility for what happened during {{ event(id="e011") }}. It's evidence **against** the accusation of us denying any wrongdoings.

I also complain about An's dishonesty and her claiming a server as her own safe space.

{{ rimage(ids="gaslighting7") }}

Again, a joke taken out of context. I've been talking about it in section about headpats already. People were joking, so were Kanade. She already knows that An doesn't want headpat offers after {{ sevent(id="se003") }} and those jokes aren't such offers.

{{ rimage(ids="gaslighting8") }}

It's Kanade expressing her feelings about An leaving the server during {{ event(id="e011") }}. In Kanade's point of view, An wanted her to drop talking about headpats.

---

Going back to accusations:
- We haven't lied about not giving An headpats anymore
- There is no evidence for us lying about our intentions
- We haven't call out An on being abuser until our second response to Ranger's report. {{ sevent(id="se012") }}
- We have actually taken partial responsibility for what happened
- Kanade having told An that she likes her happened before any insult exchanges between us.

Again, included evidence is either disconnected from accusations or accusations are disproven in other way.

## Moderation failures and solutions

{{ rimage(ids="report-ending") }}

I don't have much to comment on that section. I'll only refer to the last paragraph where Ranger is speaking about me.

Ranger says again that we have harassed An for months. Her report is repeating it a lot but doesn't prove it. It's not that surprising that moderation team didn't notice the harassment if it never happened, you know?

## Summary

To sum it up, I'll first borrow the screenshot including Az's summary again.

{{ rimage(ids="stop3") }}

He says that:

- If someone asks you to not {speak to them, interact with them, treat them} in a certain way, you don't do it
- If someone asks you to roleplay with them (roleplay is also against the rules), you don't do it
- People do not owe you explanation of why they do not wan to be spoken to or interacted with in a certain way, and continuing to do so against their wishes is harassment whether intentional or not
- If someone has asked you in the past to not interact with them in a certain way, use that as a general guideline and don't get bogged don with specifics. If someone asks you to not interact with them in a certain way, you should avoid interacting with them in similar ways too e.g. if someone asks you not to offer them hugs, consider that they may be uncomfortable receiving affection from strangers and to not be affectionate to them in any form. This may require using your brain.

I can't help but agree with what Az said here. Those point don't fit our relationship with An at all though.

- After Kanade has been told that her headpats make An feel bad, she stopped headpatting her. After Kanade has been told not to talk about headpats directed at An at all, she also stopped.
- Roleplay between us and An (which consists of more than just Kanade's headpats) was mutual and wasn't initiated only by us. Some silly talks have been initiated by her, she's been joining to talks where Kanade has been roleplaying headpats with others on her own volition. She also has been asking us about things related to headpats on her own initiative.
- People don't owe us explanation but such an explanation can help bring clarity to their real intentions. In our case with An, we have been feeling that she is hiding her real expectations as she's been getting angry at us despite us obeying boundaries drawn by her from our point of view. She's never told us "don't mention headpats [unrelated to me] to me at all". Also, she's been able to mention headpats on her own volition e.g. just before the dramas from {{ event(id="e011") }}. We asked her for explanation hoping it's going to clarify things between us.
- Kanade hasn't been affectionate towards An in messages that caused the dramas in {{ event(id="e010") }} and {{ event(id="e011") }}.

Other than that, the evidence of our supposed abuse on An consists of:
- Depictions of harmless actions (like most of the screenshots in collection about headpats)
- Depictions of mutual harassment between us and An
- Depictions of An or other people accusing us of the same things as Ranger's report

There are no actual proofs of any wrongdoings at our side. And if we look at events from screenshots in further context as in [evidence](/events) we collected:
- Us talking to An about headpats haven't breach boundaries she's drawn.
- We have consistently listened to An's feedback and adapted our behavior to her boundaries drawn.
- There is no proof of any accusation regarding boundary violations unrelated to headpats. There is a proof of An falsely accusing us of leaking her DM though.
- There is also no proof of us showing any kind of abusive behavior included in gaslighting. There are only few instances of mutual harassment between us and An. On the other hand there is a lot of evidence of An showing plenty of abusive behaviors that gaslighting consists of in {{ event(id="e007") }}, {{ event(id="e008") }}, {{ event(id="e010") }}, {{ event(id="e011") }}.

I dare to say that Ranger's report is defaming me in its entire volume. It doesn't prove its accusations, it just repeats them, just as An has repeated her claim about us "harassing her with headpats for months". Its target is not to show you the truth but to make you believe in certain narrative which isn't supported by evidence.