+++
title = "Evidence on the case"
sort_by = "date"
template = "evidence.html"
+++

Pages marked <span class="has-text-primary">like this</span> contain evidence mentioned in Ranger's report. Pages describing other events are marked <span class="has-text-info">like that</span>.