+++
title = "An asks about headpats and gets triggered"
date = "2021-10-13"
[extra]
report_event = true
+++

{{ uimage(ids="u007_1,u007_2") }}

Shierudo asks me or Kanade if headpats are sexy. We deny it. Anubis asks Kanade for a headpat and An encourages me to headpat him instead. Kanade says that she is a headpat monster, An calls her gatekeeper. Kanade tells her that she can ask me for a headpats though as she isn't getting them from Kanade anymore. Anubis is trying to give An a headpat voucher from Kanade but she says that they are named. An says that she has a no headpat policy. Kanade says that she has given An a headpat voucher before and it's invalidated already which An seems to approve at start. Also, Kanade says that perverts who find headpats sexual (it's directed to shierudo) won't get her headpats either. An suddenly accuses Kanade of harassing her for headpats for months and not taking no for an answer, calling Kanade a creep. 

{{ uimage(ids="u007_3,u007_4") }}

Aki asks An if she was meaning that Kanade did all of those things, An is asking if Kanade is too afraid to reply herself and feeling guilty. Aki says that An hurt Kanade's feelings, An is threatening us to call mods next time we put her and headpats in one sentence, then accuses Kanade of harassment. Then she is minimizing Kanade's feelings. 

Kanade is trying to compromise by saying that she might have been too clingy in the past with headpat related stuff but she hasn't done anything wrong today.

---

Original screenshots.

{{ rimage(ids="r007_1,r007_2") }}

It's An who first started talking to Kanade about headpats, telling her to let me do the headpats too. To be fair, Kanade is a little mean to An here, first saying that she won't give her headpats and then that a voucher she has given her once has been invalidated. The fragment about perverts not getting headpats is directed at shierudo's earlier question, not at An.

It's An who says that Kanade harassed her for months with headpats and doesn't take no for an answer. So far all of events included in the report state otherwise - Kanade has clearly complied to boundaries drawn by An, contrary to what An said here. It's also An who mentioned headpats to Kanade first and first have been slightly mean to her, calling Kanade a gatekeeper. Also, An seems to have snapped after a message about perverts not receiving headpats which was directed at shierudo, not An, rather than after a borderline offer on headpats from me. A borderline offer coming up after An asked for me headpatting another user.

After the second message, An says:
> From now on I will be tagging mods whenever she put me and headpat in one sentence because I did ask her a hundred times so far not to associate me with that

With the latter part (from section about Kanade refusing to stop), she is exaggerating _a bit_ and lying about having asked Kanade to not associate her with headpats at all. In {{ sevent(id="se003") }} An told us that she feel bad refusing headpats offers, not with joking about headpats related to her.

Nevertheless, An has drawn stricter boundaries here. It went from "I don't want you to impose your headpats on me" to "I don't want you to associate me with headpats at all". From this point in time, teasing An with "no headpats for you" stuff clearly violates her boundaries.  It's okay that An has drawn stricter boundaries, we have never shamed her for having them. But previous messages from Kanade cannot be stated as an evidence for violating boundaries drawn _after_ that.

An owning her boundaries doesn't change the fact that in this event she:
- Insulted Kanade calling her a creep
- Lied about Kanade harassing her for headpats for months.
- Emotionally abused Kanade making her feel guilty.
- Minimized Kanade's feelings