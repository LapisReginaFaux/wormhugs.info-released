+++
title = "Alexandra's crusade"
date = "2022-03-24T22:00:00Z"
+++

{{ simage(ids="s005_1") }}

It's first Alexandra's message after An left the server. She starts with reminding us about An asking us to stop aiming headpats/hugs stuff at her multiple times and insisting that Kanade's question about how much is 500 BTC in worm hugs was an example of such aiming.

She accuses us of implying An having unreasonable boundaries while she was very clear that it was about hugs/headpats being aimed at her. 

{{ simage(ids="s005_2") }}

Here Alexandra states that with a certain implication, it was offering hugs to An. Kanade denies that implication, saying that An has never said that "500 BTC penalty" is supposed to go to her. 

Alexandra questions Kanade about what she thinks have upset An.

{{ simage(ids="s005_3") }}

Kanade replied that An was taking any _h-words_ written by her personally.

Alexandra restated that An was upset because Kanade has been aiming h-words at her. She accuses Kanade of conveniently not reading what An has written to her, in a way that makes An look unreasonable.

Alexandra also questions Kanade's reading comprehension skills, later saying that she is not mean by that but just baffled.

{{ simage(ids="s005_4") }}

Scarlet agrees with Alexandra saying that we have a prior history of ignoring An's requests about how to interact with her. She says we should have recognized it being perceived as direct and apologized. Defending makes it looking intentional.

Alexandra said that she's seen no attempt to clarify and instead an attempt to mae An ask for something unreasonable.

{{ simage(ids="s005_5") }}

Kanade explains that from our point of view An asking to not attempt to do h-words with her or mentioning h-words related to her was a reasonable requests. But requests not to mention h-words at all [while talking to An] are another kinds of requests. She clarifies once again that she's replied to silly joke with another silly joke.

Alexandra accuses Kanade of making it look like An wanted no mentions of hugs and headpats in public in general and not clarifying, pointing at these messages: 

---

{{ simage(ids="s005_6") }}

Here Kanade vents her frustrations about An. She says that if An gets tics each time she sees hugs and headpats, we doubt that keeping in mind just our interactions with her would solve the problem. 

She notices that at first An has just asked to not headpat her. But it wasn't enough.

Kanade says that she stopped mentioning possibility of doing h-words to her and avoided talking to her directly about it (I think we meant not @mentioning her here). And today she just mentioned BTC to worm hug conversion ratio. And if _that_ triggers An, what doesn't?

She then calls out on An claiming the server being her _safe space_ as she admitted to it.

---

{{ simage(ids="s005_7") }}

Kanade replies to Anubis that if she knew about An's special circumstances, she would be careful not to o hurt her even if it wasn't Kanade's fault. She asks if it was really that hard for An to say that she is hurt by h-words?

Anubis says that it doesn't matter if An was traumatized, what matters is that if they ask you to not interact with them in certain way, you should just respect their boundaries as it's basic human decency.

Kanade says that some boundaries might be restrictive and hard to understand without explanation.

Anubis says that the freedom to interact with someone is not given but it's a privilege allowed by that person.

{{ simage(ids="s005_8") }}

Scarlet says that forcing one to explain oneself can violate their privacy in traumatizing way.

Alexandra demands another answer for her earlier question.

{{ simage(ids="s005_9") }}

Rusty says that when someone is having an issue with someone else roleplaying in the same room, they are a sensitive asshole. **Alexandra agrees with him and says that I have been doing _it_ at An repeatedly.**

**She says An was clear that doing it publicly is fine as long as it's not at An.**

Kanade makes another response. She says that it really feels for her that An is triggered by any mention of h-words regardless if it's aimed at her.

{{ simage(ids="s005_10") }}

Alexandra asks us if An perceived Kanade's comment as being a hug directed at her.

Kanade replies that she isn't sure but she definitely hasn't targeted An.

Alexandra asks Kanade if she literally didn't read anything An said multiple times.

Alexandra says that there are two possibilities:
1. Kanade not reading/being able to read messages
2. Kanade deliberately misunderstanding

{{ simage(ids="s005_11") }}

Shierudo says that it's 2. as nobody could misinterpret An's constant asking to stop.

Alexandra says that she doesn't know for sure but agrees with them. And advises us to change how we engage with people if it's 1.

She demands moderator movement about the case.

{{ simage(ids="s005_12") }}

Kanade says that maybe she is stupid or lacks empathy but she didn't intend to target An with hugs and feels she didn't do anything wrong on that day. 

Alexandra accuses us of not paying any attention to what An says and making her look like she's unreasonable. She says that all of our misunderstanding seem to lead us to being victims and others being unreasonable. She keeps asking us to clarify and we keep going back to minute detail of specific messages. She accuses us of ignoring the entire context. We supposedly roleplayed like An had problems with talking about hugs publicly.

Kanade says she is lost about if An has a problem with being targeted by hugs or being told about them.

{{ simage(ids="s005_13") }}

Alexandra says she doesn't believe that we don't get what the problem is after months and multiple instances of dramas.

Everything we post looks like a deliberate attempt to make An look unreasonable. She thinks we do it because it makes us look good in comparison. Or we have serious reading comprehension/attention issues.

Kanade points out 2 possible cases (she later stroke-through the second one, coerced by Alexandra):
1. An requested to not target her with hugs. And An misunderstood Kanade's message.
2. An requested to not speak to her about headpats at all. Her request was untenable.

Alexandra ridicules case 2.

{{ simage(ids="s005_14") }}

Kanade says that she's just quoted An telling her to not talk _about_ hugs to her. She says in next messages that at least it looks like that to her that it's case 2.

Later she's withdrawing it though seeing another An's message saying something different. She was lost at that moment.

{{ simage(ids="s005_15") }}

Zen calls out on Alexandra's leading questions. From his point of view, An's boundary is not wanting to be in a conversation when someone brings acts of cutesy affection and such a boundary is untenable. He states in the end that it's An's responsibility to deal with her triggers.

Kanade says that it would be nice if An shared having such triggers if she wanted us to proactively avoids misunderstandings.

{{ simage(ids="s005_16") }}

Alexandra is posting a screenshot of An's message when she specifies that she mean referring hugs to HER specifically. Zen replies that it was not roleplay or request for a roleplay, just referring to hugs (as not referring An with them)

Shierudo said that An has told me about her triggers many times. They claim again that An had told us, we kept ignoring her and poking at An anyway.

{{ simage(ids="s005_17") }}

Kanade admits that she could predict An having triggers by her reactions at it could be a mistake at her side that she didn't. But it's also An's responsibility to communicate clearly.

Shierudo mentions that An has been telling us "stop" multiple times.

Zen accuses shierudo of making a strawman and that pretending someone is malicious by talking about hugs in third person is the worst take he's ever seen.

Kanade says that she's stopped talking about headpats related to her and it wasn't enough.

Shierudo asks her if she's sure she did.

Kanade says that she might have slipped once or twice but she's tried.

{{ simage(ids="s005_18") }}

Alexandra accuses Zen of gaslighting and insults him. She also pings @Administrator and ask them to take action.

Luna accuses her of wanting revenge.

{{ simage(ids="s005_19") }}

Alexandra asks for what. Luna replies that no one denies that her friend got hurt.

Zen asks Alexandra who is he gaslighting and if calling Alexandra wrong is gaslighting her.

Alexandra says that she is waiting for mods.

Kanade says that she feels partially responsible for what happened.

Zen says that from his point of view An's friends have been venting on Kanade and trying to defend An's untenable actions.

{{ simage(ids="s005_20") }}

Marissa says that they agree with Zen but Kanade could have said "sorry" to defuse some tension.

Kanade says that she thinks she was being clear enough about her intentions but agrees that it could have helped.

Kanade thanks Zen for defending her.

{{ simage(ids="s005_21") }}

Marissa summarizes the situation:
1. An gets triggered and lashes out
2. Kanade gets defensive as from her point of view she wasn't roleplaying at An
3. Zen agrees that Kanade didn't overstep boundaries
4. Alexandra and Shierudo get angry at Kanade because she knew that hugs make An uncomfortable

Kanade says that she knew that associating An with hugs makes her uncomfortable. But she hasn't realized that it triggers her and how easy is to provoke misunderstandings. And admits to insisting on An explaining her behavior.

{{ simage(ids="s005_22") }}
{{ simage(ids="s005_23") }}
{{ simage(ids="s005_24") }}

I asked why is it bad to ask An for explaining her reasons. Zen explains that explaining would lead to immersing in it. I can't really relate to that.

{{ simage(ids="s005_25") }}
{{ simage(ids="s005_26") }}

Discussion wraps up for now and we reach Az's summary included in Ranger's report.

---

Alexandra tries to sabotage us and make An look like a victim. We are not handling that good. One of the reasons for that is we were not ready to face it from her. As Kanade said in {{ event(id="e006") }}, she considered Alexandra her friend and didn't expect being betrayed like that.

Alexandra insists that we were aiming at An with hugs, trying to force quite a farfetched implication. She question Kanade's reading comprehension skills multiple times. She puts a narration about Kanade sabotaging An and accused Kanade of making it look like she has made up An wanting no mentions of hugs in general. 

Alexandra was very concerned if people could start to believe that An has really wanted to censor talking about headpats in public places. She wasn't that concerned when An wanted people to believe that Kanade has been abusing An for months with headpats.

