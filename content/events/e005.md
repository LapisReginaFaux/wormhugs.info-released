+++
title = "Cat videos vs headpats; Alexandra the Great"
date = "2021-09-20"
[extra]
report_event = true
+++

{{ uimage(ids="u005_1") }}

The "Deleted User" here is me, as you can see in original screenshots included in Ranger's report in the bottom. It started from me complaining on [a certain good for nothing minister in my country](https://en.wikipedia.org/wiki/Przemys%C5%82aw_Czarnek), who had said something about some "facts" about gender being known for billions of years (and therefore, humanity existing for billions of years). An started joking about living for trillions of years and I asked her if she lived in the previous universe (as current one is not that old) and if she is responsible for the current one being so fucked up.


{{ uimage(ids="u005_2") }}

Kanade than says that An probably can't fix current universe as time travel in movies always make things end up worse. She also says that she is still going to like An even if she is responsible for the current state of universe.

{{ uimage(ids="u005_3") }}

An says that thanks to her cat videos are the thing in our timeline, Kanade says that she doesn't have to make things up for Kanade to like her. And that cat videos are as valuable for her as headpats are valuable for An.

{{ uimage(ids="u005_4") }}

Here is a silly talk about Alexandra being a reincarnation of Alexander the Great and Kanade wondering if the latter liked headpats.

---

Original screenshots from the report:

{{ rimage(ids="r005_1,r005_1a") }}

The first one (split into two parts in report) has appeared as an evidence for us gaslighting An. In reality, it was a silly talk, example of what An herself mentioned in previous event. Kanade is affectionate towards An here and there is no basis for assuming it being fake or ill-intended.

{{ rimage(ids="r005_2") }}

The second one is supposed to be example of Kanade targeting An with headpats. Kanade was saying here that she doesn't care about cat videos just like An doesn't care about her headpats. It wasn't an offer to give An a headpat.

{{ rimage(ids="r005_3") }}

The third one is Kanade talking about headpats related to Alexandra, not An.