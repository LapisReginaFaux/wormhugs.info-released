+++
title = "Actual headpats and unsolicited friend request"
date = "2021-08-22"
[extra]
report_event = true
+++

The first event includes our first interactions with An that have been included in Ranger's report.

{{ uimage(ids="u001_1,u001_2") }}

An have challenged people to try to make her block them. Kanade, taking this challenge jokingly, gives her a hug, then next day a headpat. An doesn't give Kanade any negative feedback regarding that.

{{ uimage(ids="u001_3") }}

Also, we have sent An a friend request. We have already had her other account in our friend list by then. Also, we have been joking about how ignoring equals blocking. The joke comes from `/ignore` command that's used for blocking people in IRC chats.

{{ uimage(ids="u001_4") }}

Next day, Kanade hugs An again. Less carefully than Alexandra, since An doesn't have horns in her form. Again, no negative feedback from An.

{{ uimage(ids="u002_1,u002_2") }}

Here, we are talking with An and a few other people about narcissism. 

{{ uimage(ids="u002_3") }}

Kanade gives a headpat to Alexandra. Not An.

{{ uimage(ids="u002_4") }}

An responds to Kanade's comment about narcissists liking headpats with that they might not want them. 

She might have meant that she doesn't want them but it wasn't obvious for us at that point. Still, Kanade takes a hint to ask for consent first next time she'd like to give An a headpat instead of just doing it.

{{ uimage(ids="u001_5,u001_6") }}

A day later when An starts discussion about mansplaining and asks people to stop mansplaining her (no one has been mansplaining her), Kanade offers her a hug instead. It was simply ignored.

---

Original screenshots from the report:
 
{{ rimage(ids="r001_1,r001_2") }}

First two are introduced as an evidence for _threatening to send unsolicited friend request_. Well, the friend request was simply sent. I can't really prove it at this point, but I used to have An's other account in my friend list that day. And even if I didn't, since when sending someone a friend request is a crime?

{{ rimage(ids="r001_3|r001_4|r001_5") }}

The other three are supposed examples of Kanade abusing An with headpats. As I said, An hasn't given Kanade any feedback about headpats bothering her before Kanade send those marked messages. Furthermore, in the second screenshot from this group it's not An who gets a headpat. 

There is also no basis for assuming Kanade having any malicious intentions while playing with An.

At some point An gives Kanade a hint that she shouldn't headpat people without their consent which Kanade listens to as all headpats Kanade made happen here, in {{ event(id="e001") }}.

Also, KitKat admits in this place that both she and her host might have some narcissistic tendencies.