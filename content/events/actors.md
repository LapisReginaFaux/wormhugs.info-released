+++
title = "Actors in the case"
date = "2021-01-01"
+++

I'm going to introduce most significant actors in the case as some of us use a lot of nicknames.

## Mon (me) and my tulpas

I'm a tulpamancer with 9 tulpas. A few of us are involved in the case, specifically:
- **Mon**/**Felix**
- **Kanade**
- **Nao**/**Sora**
- **Lapis**/**Freya**
- **Aki**
- **Luna**

Unfortunately, I've deleted two of my accounts, including my main account, after the case, so there is a lot of messages by **Deleted User** that belong to us.

**Faux** is like our family name. Also, we've included some of our recent incognito (mis)adventures. **Alexander** is me, **Alexia** is Sora.

## An

An is a person with DID who wants to integrate. **An**/**proxi** is a name for both host and her as a whole afaik. There are also two of her alters involved in the case:
- **Leiko**
- **KitKat**

For a while she's been using **okidokime** nickname incognito.

I'm going to mostly refer to all of her system as An.

She also deleted one of her accounts (precisely, one that belonged to Leiko), so some messages from **Deleted User** belong to her too.

## Simulacra

An's friends, **Alexandra** and **Cerys** are tulpas from the same system. They have also been using **Simulacra** as a system name.

## shierudo

Another An's friend, **shierudo** is a non-binary tulpa, I guess and their host was absent. I'm not sure of details of their system, I wasn't interacting with them a lot.

## Ranger

**Ranger** is a tulpa in quite a big system. She's the only significant actor from her system in the case though. She is a mod in tulpa.info.

## Pleeb

**Pleeb** is the owner of tulpa.info.

## Reguile

**Reguile** is another admin of tulpa.info.
